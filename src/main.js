import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import Complaints from './components/Complaints';
import ComplaintView from './components/ComplaintView';
import Register from './components/Register';
import Login from './components/Login';
import NewComplaint from './components/NewComplaint';
import About from './components/About';
import Profile from './components/Profile';
import TopComplaints from './components/TopComplaints';
import AdminLogin from './components/admin/AdminLogin';
import Dashboard from './components/admin/Dashboard';

const Main = props => (
    <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/complaints" component={Complaints}/>
        <Route exact path="/complaints/new" component={NewComplaint}/>
        <Route exact path="/complaints/top10" component={TopComplaints}/>
        <Route exact path="/complaints/:id" component={ComplaintView}/>
        <Route exact path="/register" component={Register}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/about" component={About} />
        <Route exact path="/profile" component={Profile}/>
        <Route exact path="/admin" component={AdminLogin}/>
        <Route exact path="/admin/dashboard" component={Dashboard}/>
        {/* <Route path='/register' component={Registration}/>
        <Route exact path='/users/:id' component={User}/>
        <Route path='/users/:id/update' component={Update}/> */}
    </Switch>
)

export default Main;
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";

class Header extends Component {
 
  logout = () => {
    localStorage.removeItem("jwtToken");
    toast.success("Logged out!", {});
    window.location.reload();
  };

  render() {
    return (
      <nav className="navbar navbar-expand-md navbar-light">
        <div className="container">
          <a className="navbar-brand">
            ComplaintBox
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link" to={"/"}>
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/complaints"}>
                  Complaints
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/complaints/new"}>
                  New Complaint
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/about"}>
                  About
                </Link>
              </li>
            </ul>

            {localStorage.getItem("jwtToken") ? (
              <ul className="navbar-nav justify-content-end">
                <li className="nav-item">
                  <Link className="nav-link profile-link" to={"/profile"}>
                  <i className="fas fa-user"></i>
                    Profile
                  </Link>
                </li>
                <li className="nav-item" > 
                  <Link className="nav-link profile-link" onClick={this.logout} to={"/"}>
                  <i className="fas fa-sign-out-alt"></i>
                    Logout
                  </Link>
                </li>
                
              </ul>
            ) : (
              <ul className="navbar-nav justify-content-end">
                <li className="nav-item">
                  <Link className="nav-link" to={"/register"}>
                    <i className="fas fa-user-plus"></i> 
                    Register
                  </Link>
                </li>
                <li>
                  <Link className="nav-link" to={"/login"}>
                    <i className="fas fa-sign-in-alt"></i> 
                    Login
                  </Link>
                </li>
                
              </ul>
            )}
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;

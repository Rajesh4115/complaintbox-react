import React, { Component } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import complaintSvg from '../imgs/complaint.svg';

class ComplaintView extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      title: '',
      category: '',
      description: '',
      votes: ''
    }
    console.log();
    
    fetch(`/complaints/${this.props.match.params.id}`)
      .then(res => res.json())
      .then(response => this.setState(response.obj))
      .catch(error => console.error(error));
  }

  handleUpvote = () => {
    const url = `/complaints/${this.props.match.params.id}/votes`;
    axios
      .patch(
        url,
        {},
        {
          headers: { Authorization: localStorage.getItem("jwtToken") }
        }
      )
      .then(result => {
        toast.success(result.data.message);
        this.props.history.push("/complaints");
      })
      .catch(error => {
        if (error.response.status === 401) {
          toast.error("Please log in to upvote a complaint!", {
            position: "bottom-right",
            hideProgressBar: true
          });
        } else if (error.response.status === 500) {
          toast.info(error.response.data.error.message);
        }
      });
  };
  render() {
    return (
      <div className="container complaint">
        <h2>Complaint Details</h2>
        <hr />
        <div className="container row">
          <img src={complaintSvg} alt="Complaint" className="col-md-4 complaint__svg" />
          <dl className="col-md-8 row complaint-view__dl">
            <dt className="col-sm-4">Title</dt>
            <dd className="col-sm-8">{this.state.title}</dd>
      
            <dt className="col-sm-4">Category</dt>
            <dd className="col-sm-8">{this.state.category}</dd>

            <dt className="col-sm-4">Description</dt>
            <dd className="col-sm-8">
              {this.state.description}
            </dd>

            <dt className="col-sm-4">Posted on</dt>
            <dd className="col-sm-8">
              {(new Date(this.state.created)).toDateString()}
            </dd>
          </dl>
        </div>
        <button className="complaint__upvote" onClick={this.handleUpvote} >
        <span className="complaint__upvote-count">{this.state.votes}</span>
          <i className="fas fa-thumbs-up fa-3x"></i> 
          
        </button>
          

      </div>
    );
  }
}

export default ComplaintView;

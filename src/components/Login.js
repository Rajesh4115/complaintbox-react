import React, { Component } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";

import { Link } from "react-router-dom";

const styles = {
  floatingLabelStyle: {
    color: "black"
  },
  floatingLabelFocusStyle: {
    color: "cyan"
  }
};

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: ""
    };
  }
  onChange = e => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  };

  onSubmit = e => {
    e.preventDefault();

    const { email, password } = this.state;

    axios
      .post("/users/signin", { email, password })
      .then(result => {
        localStorage.setItem("jwtToken", result.data.token);
        localStorage.setItem("userName", result.data.name);
        localStorage.setItem("userType", result.data.type);
        toast.success("Login Successful!");
        this.props.history.push("/");
      })
      .catch(error => {
        if (error.response.status === 401) {
          toast.error("Login failed. Username or password does not match");
        }
      });
  };

  render() {
    const { email, password } = this.state;
    return (
      <div className="container login">
        <form className="form" onSubmit={this.onSubmit}>
          <h2 className="form-heading">Please sign in</h2>
          <TextField
            type="email"
            hintText="E-Mail"
            floatingLabelText="E-Mail"
            name="email"
            value={email}
            onChange={this.onChange}
            required
            floatingLabelStyle={styles.floatingLabelStyle}
            fullWidth={true}
          />

          <TextField
            type="password"
            hintText="Password"
            floatingLabelText="Password"
            name="password"
            value={password}
            onChange={this.onChange}
            floatingLabelStyle={styles.floatingLabelStyle}
            fullWidth={true}
            required
          />
          <div className="login__form-submit">
            <RaisedButton type="submit" label="Login" primary={true} />
            <p className="login-footer">
              Not a member?{" "}
              <Link to="/register">
                <span
                  className="glyphicon glyphicon-plus-sign"
                  aria-hidden="true"
                />Register here{" "}
              </Link>
            </p>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;

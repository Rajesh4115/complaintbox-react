import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return( 
            <footer className="footer">
                Designed &amp; Developed by : Rajesh Sharma
            </footer>
        )
    }
}

export default Footer;
import React, {Component} from 'react';

class About extends Component {
    render() {
        return(
            <div className="container">
                <h3>About</h3>
                <p>
                Complaint Box is a website portal for the complaints regarding college which can be categorized on the basis of the authorities and departments. The complaints would either be public or private. The former one would be up voted and on the basis of that the complaint with maximum votes would be send to the respective authorities every 15 days. However, the later one would consist of emergency private complain which would be directly mailed to the authority. The college students can register to the website by submitting basic credentials such as name, registration Id, an image of college ID card, unique user name and can set up a password for further login. They can then login with the unique user name and the password as set to lodge or up vote complain. To increase the voting on complain and to get it in sight of the authorities, the students can share it with their friends which would lead to the confirmation that this is the most urgent problem to be solved.
                </p>
            </div>
        )
    }
}

export default About;
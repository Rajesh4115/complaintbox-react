import React, { Component } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import ComplaintCard from "./ComplaintCard";

import avatar from '../imgs/user.svg';
class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      complaints: []
    };
    if (!localStorage.getItem("jwtToken")) {
      toast.info("Please log in to post a complaint!");
      this.props.history.push("/login");
    }

    axios
      .get("/users", {
        headers: { Authorization: localStorage.getItem("jwtToken") }
      })
      .then(response => this.setState({ 
          user: response.data.user,
          complaints: response.data.complaints
        }))
      .catch(error => console.error(error));
  }

  // TODO: Design it properly
  
  render() {
    return (
      <div className="container">
        <h2 className="profile__head">{this.state.user.name}</h2>
        <hr />
        <div className="container profile row">
            <img src={avatar} alt="avatar" className="profile__avatar col-sm-4"/>
          <dl className="row col-sm-8 profile__dl">
            <dt className="col-sm-5">Email</dt>
            <dd className="col-sm-6">{this.state.user.email}</dd>

            <dt className="col-sm-5">Mobile</dt>
            <dd className="col-sm-6">{this.state.user.mobile}</dd>

            <dt className="col-sm-5">Admission No.</dt>
            <dd className="col-sm-6">{this.state.user.admission_no}</dd>

            <dt className="col-sm-5">Department</dt>
            <dd className="col-sm-6">{this.state.user.department}</dd>

            <dt className="col-sm-5">Semester</dt>
            <dd className="col-sm-6">{this.state.user.semester}</dd>
          </dl>
        </div>
        <hr/>
        <h2 className="profile__complaints-head">My Complaints</h2>
        <div className="container row">
            {this.state.complaints.map(complaint => (
                <ComplaintCard key={complaint._id} complaint={complaint} />
          ))}
        </div>
      </div>
    );
  }
}

export default Profile;

import React, { Component } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import TextField from "material-ui/TextField"; 

import "./admin.css";

const styles = {
  floatingLabelStyle: {
    color: "black"
  },
  floatingLabelFocusStyle: {
    color: "cyan"
  }
};

class AdminLogin extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: ""
    };
  }
  onChange = e => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  };

  onSubmit = e => {
    e.preventDefault();

    const { username, password } = this.state;

    axios
      .post("/admin/login", { username, password })
      .then(result => {
        localStorage.setItem("jwtToken", result.data.token);
        toast.success("Login Successful!");
        this.props.history.push("/admin/dashboard");
      })
      .catch(error => {
        if (error.response.status === 401) {
          toast.error("Login failed. Username or password does not match");
        }
      });
  };

  render() {
    const { username, password } = this.state;
    return (
      <div className="login admin__login">
        <div className="admin__form-heading">
          Please sign in
        </div> 
        <form className="admin__login__form" onSubmit={this.onSubmit}>
          <div className="admin__login__form-group">
          <TextField
            type="text"
            hintText="Username"
            floatingLabelText="Username"
            name="username"
            value={username}
            onChange={this.onChange}
            required
            fullWidth={true}
            floatingLabelStyle={styles.floatingLabelStyle}
          />

          <TextField
            type="password"
            hintText="Password"
            floatingLabelText="Password"
            name="password"
            value={password}
            onChange={this.onChange}
            fullWidth={true}
            floatingLabelStyle={styles.floatingLabelStyle}
            required
          />
          </div>
          <div className="admin__login__form-submit">
            <input type="submit" value="Login"/>
          </div>
        </form>
        
      </div>
    );
  }
}

export default AdminLogin;

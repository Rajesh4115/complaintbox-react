import React, {Component} from 'react';
import ComplaintCard from "./ComplaintCard";
import RaisedButton from "material-ui/RaisedButton";
import axios from "axios";
import { toast } from "react-toastify";

import complaintSvg from '../../imgs/complaint.svg';

class Dashboard extends Component {
    constructor() {
        super();
        
        this.state = {
            filter: "",
            complaints: [],
            show: "list",
            selectedComplaint: {}
        }
    }

    showDetails = (complaint) => {
        console.log(complaint);
        this.setState({
            complaint: complaint,
            show: "details"
        })
    }

    componentDidMount() {
        fetch("/admin/complaints")
          .then(res => res.json())
          .then(response => this.setState({ complaints: response.obj }))
          .catch(error => console.error(error));
    }

    showAll = () => {
        fetch("/admin/complaints")
          .then(res => res.json())
          .then(response => this.setState({ complaints: response.obj, show: 'list', filter: "all" }))
          .catch(error => console.error(error));
    }

    showPublic = () => {
        fetch("/admin/complaints/public")
          .then(res => res.json())
          .then(response => this.setState({ complaints: response.obj, show: 'list', filter: "Public"  }))
          .catch(error => console.error(error));
    }

    logout = () => {

    }

    showPrivate = () => {
        fetch("/admin/complaints/private")
          .then(res => res.json())
          .then(response => this.setState({ complaints: response.obj, show: 'list', filter: "private" }))
          .catch(error => console.error(error));
    }


    markResolved = () => {
        const url = `/admin/complaints/resolve/${this.state.complaint._id}`;
        axios
            .post(
            url,
            {},
            {
                headers: { Authorization: localStorage.getItem("jwtToken") }
            }
        )
        .then(result => {
            toast.success(result.data.message);
            this.props.history.push("/admin/dashboard");
            this.setState({
                show: "list"
            })
        })
        .catch(error => {
            if (error.response.status === 401) {
            toast.error("Please log in to continue!", {
                position: "bottom-right",
                hideProgressBar: true
            });
            } else if (error.response.status === 500) {
                toast.info(error.response.data.error.message);
            }
      });
    }

    render() {
        return(
            <div className="dashboard">
                <div className="nav-side-menu">
                    <i className="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
            
                    <div className="menu-list">
            
                        <ul id="menu-content" className="menu-content collapse out">
                            <li onClick={this.showAll}>
                            <a >
                            <i className="fas fa-list-alt "></i> All Complaints
                            </a>
                            </li>

                            <li onClick={this.showPublic}>
                            <a >
                            <i className="far fa-eye "></i> Public Complaints
                            </a>
                            </li>

                            <li onClick={this.showPrivate}>
                            <a >
                            <i className="far fa-eye-slash "></i> Private Complaints
                            </a>
                            </li>

                            {/* <li>
                            <a href="#">
                            <i className="fa fa-users "></i> Users
                            </a>
                            </li> */}

                            <li onClick={this.logout}>
                            <a >
                            <i className="fas fa-sign-out-alt"></i> Logout
                            </a>
                            </li>
                        </ul>
                    </div>
                </div>
            
                
                {this.state.show === "list" && (
                    <div className="container dashboard__wrapper">
                        <h2><span className="dashboard__filter">{this.state.filter}</span> Complaints</h2>
                        <hr/>
                        <div className="row">
                            {this.state.complaints.map(complaint => (
                                <ComplaintCard key={complaint._id} complaint={complaint} showDetails={this.showDetails}/>
                            ))}
                        </div>
                    </div>
                )}
                {this.state.show === "details" && (
                    <div className="container dashboard__wrapper">
                        <h2>Details</h2>
                        <hr/>
                        <div className="container row">
                            <img src={complaintSvg} alt="Complaint" className="col-md-4 complaint__svg" />
                            <dl className="col-md-8 row complaint-view__dl">
                                <dt className="col-sm-4">Title</dt>
                                <dd className="col-sm-8">{this.state.complaint.title}</dd>
                        
                                <dt className="col-sm-4">Category</dt>
                                <dd className="col-sm-8">{this.state.complaint.category}</dd>

                                <dt className="col-sm-4">Description</dt>
                                <dd className="col-sm-8">
                                {this.state.complaint.description}
                                </dd>

                                <dt className="col-sm-4">Posted on</dt>
                                <dd className="col-sm-8">
                                    {(new Date(this.state.complaint.created)).toDateString()}
                                </dd>
                            </dl>
                        </div>
                        <RaisedButton className="dashboard__resolved-btn" label="Mark as resolved" primary={true} onClick={this.markResolved}/>

                    </div>
                )}
                
            </div>
        )
    }
}

export default Dashboard;
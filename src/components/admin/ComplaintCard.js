import React, { Component } from "react";
import { Link } from "react-router-dom";
import RaisedButton from "material-ui/RaisedButton";

class ComplaintCard extends Component {
   
  render() {
    return (
      <div className="col-md-6 col-lg-4" style={{ marginBottom: "20px" }}>
        <div className="card" style={{ width: "20rem" }}>
          <div className="card-body">
            <h5 className="card-title">{this.props.complaint.title}</h5>
            <h6 className="card-subtitle mb-2 text-muted">
              {this.props.complaint.category}
            </h6>
            <p className="card-text">
              {this.props.complaint.description.substring(0, 100)}...
            </p>
            <a
              className="card-link"
            >
            <RaisedButton label="Details" primary={true} onClick={() =>{this.props.showDetails(this.props.complaint)}}/>
            </a>
            <span>&nbsp;&nbsp;&nbsp;Upvotes: {this.props.complaint.votes}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default ComplaintCard;

import React, { Component } from "react";
import { Link } from "react-router-dom";

import steps from "../imgs/steps.svg";
import Footer from "./Footer";

class Home extends Component {

  // TODO: Design it properly
  render() {
    return (
      <div className="home">
      <div className="home__wrapper">
          <div className="jumbotron text-center">
            <h1>Complaint Box</h1>
            <p>All your grievances at the campus, Resolved!</p>
          </div>
          <div className="container home__cta-container row">
          <div className="home__cta-wrapper col-lg-3">
            <Link className="nav-link" to={"/register"}>
              <div className="home__cta ">
                <i className="fas fa-user-plus fa-3x"></i>
              </div>
              <h3>Register</h3>
            </Link>
          </div>
          <div className="home__cta-wrapper col-lg-3">
            <Link className="nav-link" to={"/complaints"}>
              <div className="home__cta">
                <i className="fas fa-list fa-3x"></i>
              </div>
              <h3>Browse Complaints</h3>
            </Link>
          </div>
          <div className="home__cta-wrapper col-lg-3">
            <Link className="nav-link" to={"/complaints/new"}>
              <div className="home__cta">
                <i className="far fa-edit fa-3x"></i>
              </div>
              <h3>Post a Complaint</h3>
            </Link>
          </div>
        </div>
        <hr/>
        <h3 className="home__steps-head">How does it work?</h3>
        <img className="home__steps-img" src={steps} alt="steps"/>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;

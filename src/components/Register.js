import React, { Component } from "react";
import { toast } from "react-toastify";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import Divider from "material-ui/Divider";

import axios from "axios";

const styles = {
  floatingLabelStyle: {
    color: "black"
  },
  floatingLabelFocusStyle: {
    color: "cyan"
  }
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password1: "",
      password2: "",
      email: "",
      name: "",
      mobile: "",
      admission_no: "",
      department: "",
      semester: "",
      gender: ""
    };

    this.logChange = this.logChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    if(this.state.password1.length < 8) {
      this.setState({
        password1: "",
        password2: ""
      });
      document.getElementById("password1").focus();
      toast.error("Password should be at least 8 character long.");
      return false;
    }
    if (this.state.password1 !== this.state.password2) {
      this.setState({
        password1: "",
        password2: ""
      });
      document.getElementById("password1").focus();
      toast.error("Password & Confirm password should be same.");
      return false;
    }
    if (!/^\d{10}$/.test(this.state.mobile)) {
      toast.error("Please enter a correct mobile number");
      document.getElementById("mobile").focus();

      return false;
    }
    var data = this.state;
    console.log(data);
    axios
      .post("/users/register", data)
      .then(result => {
        toast.success("Registered successfully, Please Log in to continue!");
        this.props.history.push("/login");
      })
      .catch(error => {
        toast.error(error.response.data.error.message);
      });
  }

  logChange(e) {
    console.log([e.target.name] + " | " + e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSelect = (event, index, value) => this.setState({ semester: value });

  render() {
    return (
      <div className="container register">
        <form onSubmit={this.handleSubmit} className="register-form">
          <h2>Register</h2>
          <Divider />

          <TextField
            value={this.state.name}
            onChange={this.logChange}
            name="name"
            type="text"
            hintText="Full Name"
            floatingLabelText="Full Name"
            fullWidth={true}
            required
            floatingLabelStyle={styles.floatingLabelStyle}
          />
          <TextField
            value={this.state.email}
            onChange={this.logChange}
            name="email"
            type="email"
            hintText="E-Mail"
            floatingLabelText="E-Mail"
            fullWidth={true}
            floatingLabelStyle={styles.floatingLabelStyle}
            required
          />
          <TextField
            value={this.state.password1}
            onChange={this.logChange}
            name="password1"
            type="password"
            hintText="Password"
            floatingLabelText="Password"
            fullWidth={true}
            id="password1"
            floatingLabelStyle={styles.floatingLabelStyle}
            required
          />
          <TextField
            value={this.state.password2}
            onChange={this.logChange}
            name="password2"
            type="password"
            hintText="Confirm Password"
            floatingLabelText="Confirm Password"
            fullWidth={true}
            id="password2"
            floatingLabelStyle={styles.floatingLabelStyle}
            required
          />

          <TextField
            value={this.state.mobile}
            onChange={this.logChange}
            name="mobile"
            type="number"
            hintText="Mobile"
            floatingLabelText="Mobile"
            fullWidth={true}
            id="mobile"
            floatingLabelStyle={styles.floatingLabelStyle}
            required
          />
          <TextField
            value={this.state.admission_no}
            onChange={this.logChange}
            name="admission_no"
            type="text"
            hintText="Admission No."
            floatingLabelText="Admission No."
            fullWidth={true}
            floatingLabelStyle={styles.floatingLabelStyle}
            required
          />
          <TextField
            value={this.state.department}
            onChange={this.logChange}
            name="department"
            type="text"
            hintText="Department"
            floatingLabelText="Department"
            fullWidth={true}
            floatingLabelStyle={styles.floatingLabelStyle}
            required
          />
          <SelectField
            floatingLabelText="Semester"
            name="semester"
            value={this.state.semester}
            onChange={this.handleSelect}
            fullWidth={true}
            floatingLabelStyle={styles.floatingLabelStyle}
          >
            <MenuItem value={1} primaryText="1" />
            <MenuItem value={2} primaryText="2" />
            <MenuItem value={3} primaryText="3" />
            <MenuItem value={4} primaryText="4" />
            <MenuItem value={5} primaryText="5" />
            <MenuItem value={6} primaryText="6" />
            <MenuItem value={7} primaryText="7" />
            <MenuItem value={8} primaryText="8" />
          </SelectField>
            <div className="register__form-submit">
              <RaisedButton type="submit" label="Register" primary={true} />
            </div>
        </form>
      </div>
    );
  }
}

export default Register;

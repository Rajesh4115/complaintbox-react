import React, { Component } from "react";
import { toast } from "react-toastify";
import { RadioButton, RadioButtonGroup } from "material-ui/RadioButton";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";
import Divider from "material-ui/Divider";

import axios from "axios";

const styles = {
  floatingLabelStyle: {
    color: "black"
  },
  floatingLabelFocusStyle: {
    color: "cyan"
  }
};

class NewComplaint extends Component {
  constructor(props) {
    super(props);
    if (!localStorage.getItem("jwtToken")) {
      toast.info("Please log in to post a complaint!");
      this.props.history.push("/login");
    }
    this.state = {
      type: "",
      category: "",
      title: "",
      description: ""
    };

    this.logChange = this.logChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    var data = this.state;
    console.log(data);

    axios
      .post("/complaints", data, {
        headers: { Authorization: localStorage.getItem("jwtToken") }
      })
      .then(result => {
        toast.success("Complaint posted successfully!");
        this.props.history.push("/complaints");
      });
  }

  logChange(e, index, value) {
    // this.setState({ [e.target.name]: e.target.value });
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  }

  handleSelect = (event, index, value) => this.setState({ category: value });

  render() {
    return (
      <div className="container new-complaint-wrapper">
        <form onSubmit={this.handleSubmit} className="new-complaint-form">
          <h2>Post a new Complaint</h2>

          <Divider />
          <br />
          <RadioButtonGroup
            name="type"
            defaultSelected={this.state.type}
            onChange={this.logChange}
            required
          >
            <RadioButton value="public" label="Public" />
            <RadioButton value="private" label="Private" />
          </RadioButtonGroup>
          <SelectField
            floatingLabelText="Category"
            name="category"
            value={this.state.category}
            onChange={this.handleSelect}
            floatingLabelStyle={styles.floatingLabelStyle}
            fullWidth={true}
            required
          >
            <MenuItem value="faculty" primaryText="Faculty" />
            <MenuItem value="hostel" primaryText="Hostel" />
            <MenuItem value="transport" primaryText="Transport" />
            <MenuItem value="campus" primaryText="Campus" />
            <MenuItem value="class" primaryText="Class" />
            <MenuItem value="accounts" primaryText="Accounts" />
            <MenuItem value="students-section" primaryText="Students Section" />
            <MenuItem value="maintenance" primaryText="Maintenance" />
            <MenuItem value="other" primaryText="Other" />
          </SelectField>
          <TextField
            type="text"
            hintText="Title"
            floatingLabelText="Title"
            name="title"
            value={this.state.title}
            onChange={this.logChange}
            fullWidth={true}
            required
            floatingLabelStyle={styles.floatingLabelStyle}
          />
          <TextField
            type="text"
            multiLine={true}
            hintText="Description"
            floatingLabelText="Description"
            name="description"
            value={this.state.description}
            onChange={this.logChange}
            fullWidth={true}
            required
            floatingLabelStyle={styles.floatingLabelStyle}
            rows={3}
          />
          <RaisedButton type="submit" label="Submit" primary={true} />
        </form>
      </div>
    );
  }
}

export default NewComplaint;

import React, { Component } from "react";
import ComplaintCard from "./ComplaintCard";

class Complaints extends Component {
  constructor(props) {
    super(props);
    this.state = {
      complaints: []
    };
  }
  
  componentDidMount() {
    fetch("/complaints")
      .then(res => res.json())
      .then(response => this.setState({ complaints: response.obj }))
      .catch(error => console.error(error));
  }

  render() {
    return (
      <div className="container">
        <h1>Public Complaints</h1>
        <hr/>
        <div className="row">
          {this.state.complaints.map(complaint => (
            <ComplaintCard key={complaint._id} complaint={complaint} />
          ))}
        </div>
      </div>
    );
  }
}

export default Complaints;

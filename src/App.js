import React, { Component } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import "./App.css";
import Main from "./main";
import Header from "./components/Header";

class App extends Component {
  constructor() {
    super();

    this.state ={
      isAdmin: false
    }

    
  }

  componentDidMount() {

    let path = String(window.location.pathname);
    console.log(path);
    
    if(path.indexOf("admin") !== -1) {
      this.setState({
        isAdmin: true
      })
    }
  }
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <ToastContainer
            position="bottom-right"
            autoClose={4000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
          />
          {
            this.state.isAdmin ? '': <Header />
          }
          <Main />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
